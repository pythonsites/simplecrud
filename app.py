# Python 3
# coding: utf-8

__author__ = "Ammar S Malik"
__copyright__ = "Copyright 2022, Simple CRUD application"
__credits__ = ["Ammar Malik"]
__license__ = "GPL"
__version__ = "1.0.1"
__maintainer__ = "all creditors/github"
__email__ = "dev.malik.ammar@gmail.com"
__status__ = "Production"

# Notes: 
# For templates/index.html, do not need to specify the folder. It knows where to go

# initialize Flask to run application
# render_template = To render templates
# url_for = to point to the css file in static
# request = to see what request was made in each route
from asyncio import Task, tasks
from crypt import methods
from importlib.resources import contents
from flask import Flask, redirect, render_template, url_for, request

# import SQL Alchemy to connect to a database
from flask_sqlalchemy import SQLAlchemy

# Import Date Time to record date and time
from datetime import datetime

# initialize this file as flask application
app = Flask('__name__')

# ==============================================================================================================================
# now configure your application to connect to the database. We are using SqLite as an example
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'    # Here 4 slashes means absolute path, 3 means relative path

# now initialize the database
db = SQLAlchemy(app)

# ----- Create a class with database model
class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(200), nullable=False)
    completed = db.Column(db.Integer, default=0)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    # create a function to return the task and the id 
    # __repr__ means that this will return an object as a string. Here we are returning a Task of self.id
    def __repr__(self) -> str:
        return '<Task %r>' % self.id


# ==============================================================================================================================
# All routes for this website

# ----- Index page or home for this website
# ----- This will have two methods assigned to it. POST and GET. Default route is GET
@app.route('/', methods=['POST','GET'])
def index():
    if request.method == 'POST':
        # This is a POST method which means we are going to save the contents to the database
        taskAdded = request.form['content']

        # now create a model or instanciate the class above
        newRecord = Todo(content=taskAdded)
        try:
            db.session.add(newRecord)
            db.session.commit()
            return redirect('/')
        except:
            return "error saving information"
    else:
        # This is a GET method which means get the information and show it on the screen. Here we are showing everything
        # get al records in the database and show it on the screen
        allRecords = Todo.query.order_by(Todo.date_created).all()
        return render_template('index.html', tasks=allRecords)


# ----- Edit an existing record 
# ----- This will have default route is GET and will receive a parameter which is id of the selected record from the grid
# ----- To bind the selected record, we will use task and not tasks. As there is only one record to update
@app.route('/update/<int:id>', methods=['POST','GET'])
def update(id):
    task = Todo.query.get_or_404(id)

    if request.method == 'POST':
        task.content = request.form['content']
        try:
            db.session.commit()
            return redirect('/')
        except:
            return "error editting information"
    else:
        return render_template('update.html', task=task)


# ----- Delete an existing record 
# ----- This will have default route is GET and will receive a parameter which is id of the selected record from the grid
@app.route('/delete/<int:id>')
def delete(id):
    recToDelete = Todo.query.get_or_404(id)

    try:
        db.session.delete(recToDelete)
        db.session.commit()
        return redirect('/')
    except:
        return "error deleting information"

# Main entrypoint of this website
if __name__ == "__main__":
    app.run(host="localhost",port=3000,debug=True)