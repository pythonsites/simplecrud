# SimpleCRUD

This is a Flask application as a sample/experiment to learn flask.
As a database it is using sql lite

## Reference

https://www.youtube.com/watch?v=Z1RJmh_OqeA

## Getting started

This is a simple CRUD application using Flask for Python.
All dependencies will be installed locally using virtualenv within the project 

## Setup for this app

- Install Python 3
- Follow these commands below to setup the environment

Install virtual environment
```
pip3 install virtualenv
```

Once the environment is installed, we will initialize it to a variable 'env' is the variable we are using. Normally that is used but it can be anything
After it is setup, we will activate this environment so anything we install here will not be installed to the system globally. It will be confined to the boundries of this project

```
virtualenv env
source env/bin/activate
```

After the environment is setup and ready, we will install flask and flask-sqlalchemy

```
pip3 install flask flask-sqlalchemy
```

After this environment is all set, we will start the project and start writing our code